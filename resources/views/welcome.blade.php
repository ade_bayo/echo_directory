@extends('layouts.app')

@section('content')

<section>
    <div class="header__banner"> <!-- container -->
        <div class="overlay">
            <div class = "overlay__heading h1">
                <h1>THIS IS LAGOS</h1>
            </div>
            <div class = "overlay__heading p">
                <p>Find top Tech companies where big things are happening in Africa</p> 
            </div>
            <div class="search__container">
                <form action= '/search' method = 'get'>
                    <div class="form-row align-items-center">
                        <div class="col-md-5">
                            <label class="sr-only" for="inlineFormInputName">Search by Title</label>
                            <input type="text" class="form-control mb-2 mb-sm-0" id="inlineFormInputName" placeholder="Search by name">
                        </div>
                        <div class="col-md-4">
                            <select class="form-control form-control-lg">
                                  <option>Select category</option>
                                  <option>Tech Hubs </option>
                                  <option>Tech Training </option>
                                  <option>Data Management</option>
                                  <option>Web & Software</option>
                                  <option>Android & Mobile</option>
                            </select>
                        </div>                   
                        <div class="col-md-3">
                            <button type="submit" class="btn btn-primary">Search</button>
                        </div>
                    </div>
                </form>
            </div>        



            <div class = "icons__container icons__container ">
                <div class="links">
                                  
                    <a href="http://127.0.0.1:8000/android"><span class="glyphicon glyphicon-phone" aria-hidden="true"></span>Android & Mobile</a>
                    <a href="http://127.0.0.1:8000/techservices"><span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>Web & Software</a>
                    <a href="http://127.0.0.1:8000/database"><span class="glyphicon glyphicon-cloud" aria-hidden="true"></span>Data Management</a>
                    <a href="http://127.0.0.1:8000/techtraining"><span class="glyphicon glyphicon-link" aria-hidden="true"></span>Tech Training </a>
                    <a href="http://127.0.0.1:8000/techhub"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Tech Hubs </a>
                </div>
            </div>
               
         </div>
   </div> 
</section>       

<section class="featured__categories" >
 
        <div class=" section__title">
            <h2>Featured <span class="innovation__color">Categories</span></h2> 
        </div>
        <div class="row">
            <div class="col-md-3 col-sm-3 colg=3 col-xl-3 col-xs-12 featured__container">
                <img src="images/web_software.jpg" alt="Web and Software" class="image">
                <div class="featured__overlay">
                   <a href="http://127.0.0.1:8000/techservices"><div class="text">Web & Software</div></a> 
                </div>
            </div>
            <div class="col-md-3 col-sm-3 colg=3 col-xl-3 col-xs-12 featured__container">
                <img src="images/Tech_training.jpg" alt="Tech Training" class="image">
                <div class="featured__overlay">
                   <a href="http://127.0.0.1:8000/techtraining"><div class="text">Tech Training </div></a> 
                </div>
            </div>
            <div class="col-md-3 col-sm-3 colg=3 col-xl-3 col-xs-12 featured__container">
                <img src="images/server and storages.jpg" alt="Database Management" class="image">
                <div class="featured__overlay">
                   <a href="http://127.0.0.1:8000/database"><div class="text">Data Management</div></a> 
                </div>
            </div>
            <div class="col-md-3 col-sm-3 colg=3 col-xl-3 col-xs-12 featured__container">
                <img src="images/tech_hub.jpg" alt="Tech Hub" class="image">
                <div class="featured__overlay">
                   <a href="http://127.0.0.1:8000/techhub"><div class="text">Tech Hubs</div></a> 
                </div>
            </div>
        </div>

</section>

<section class="container">
        <div class=" section__title">
            <h2>Promote your <span class="innovation__color">INNOVATION</span></h2> 
        </div>

        <div class="row">
            <div class="col-md-5 col-sm-5 colg=5 col-xl-5 col-xs-12">
                <img src="images/hardware and legos.jpg" alt="Hardware and Legos" class="featured__image">
            </div>
            

            <div class="col-md-5 col-sm-5 colg=5 col-xl-5 col-xs-12 body__text">
                <div>
                    <p >GET CONNECTED! Our mission is to help support locally run TECHNOLOGY companies throughout Nigeria. We gladly accept directory listings for legitimate local tech companies owned and operated within Nigeria. Our directory list is designed to help consumers and potential investors find local technology companies in Nigeria. <br>
                     If you are a Technology company owner or company representative, please submit your company listing information and we will review it promptly.  If approved, featured listings will appear within 48 hours. Thank you!   
                    </p>   
                </div>
                <div>
                    <a href="http://127.0.0.1:8000/registration" title="Register Now"><button type="button" class="botton btn-lg">Register Company</button></a> 
                </div>       
            </div>
       </div>
</section>
{{--  <div class="container">
    <div class="row">
        <div class="row">
            <div class="col-md-9">
                <h3>
                    Carousel Product Cart Slider</h3>
            </div>
            <div class="col-md-3">
                <!-- Controls -->
                <div class="controls pull-right hidden-xs">
                    <a class="left fa fa-chevron-left btn btn-success" href="#carousel-example"
                        data-slide="prev"></a><a class="right fa fa-chevron-right btn btn-success" href="#carousel-example"
                            data-slide="next"></a>
                </div>
            </div>
        </div>
        <div id="carousel-example" class="carousel slide hidden-xs" data-ride="carousel">
            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="item active">
                    <div class="row">
                        @foreach($companies as $company)
                        <div class="col-sm-3">
                            <div class="col-item">
                                <div class="photo">
                                    <img src="{{$company->name}}" class="img-responsive" alt="a" />
                                </div>
                                <div class="info">
                                    <div class="row">
                                        <div class="price col-md-6">
                                            <h5>
                                                {{$company->company_name}}</h5>
                                        </div>
                        
                                    </div>
                                    <div class="separator clear-left">
                                        <p class="btn-add">
                                            <i class="fa fa-shopping-cart"></i><a href="http://www.jquery2dotnet.com" class="hidden-sm">Add to cart</a></p>
                                        <p class="btn-details">
                                            <i class="fa fa-list"></i><a href="http://www.jquery2dotnet.com" class="hidden-sm">More details</a></p>
                                    </div>
                                    <div class="clearfix">
                                    </div>
                                </div>
                            </div>
                        </div>
                       @endforeach
                        
                    </div>
                </div>
                
            </div>
        </div>
    </div>
   
</div>  --}}

@endsection


