@extends('layouts.app')

@section('content')



<section id="about" class="section-content about">
        <div class="container">
        	<div class="row">
                <div class="col-md-6 col-md-offset-3 card__image">
                            {{--  <img class="card-img-top" src="{{$company->name}}" alt="company logo" height="60px" width=auto;>  --}}
        
                    <img class= "card-img-top image" src = "../company_logo/{{$company->name}}" alt="company logo" height="60px" width=auto;>
                    
                    <div class="clearfix"></div>
                    <h2 class="name"><b>{{$company->company_name}}</b></h2>
                    <h4 class="text-center location">{{$company->mobile}}</h4>
                    <h4 class="text-center location">{{$company->website}}</h4>
                    <h4 class="text-center location">{{$company->company_email}}</h4>
                    <div class="title-divider">
                        <span class="hr-divider col-xs-5"></span>
                        <span class="icon-separator col-xs-2"><i class="fa fa-star"></i></span>
                        <span class="hr-divider col-xs-5"></span>
                    </div>

                </div>
                
                <div class="col-md-8 col-md-offset-2 text-center">
                    <p class="caption">{{$company->business_details}}</p>
                    <h2 class="slogan">{{$company->services}}</h2>

                    <a href ="/companies" class= "btn btn-danger ">Go back</a> 
                    <a href="#" class="btn btn-default page-scroll primary-bg btn-lg">Request Service</a>
                    @if (! Auth::guest())
                        @if(Auth::user()->id == $company->user_id)
                            <div>
                                <a href="/companies/{{$company->id}}/edit" class="btn btn-default secondary-bg btn-lg">Edit</a>   
                                {!!Form::open(['action'=>['AdminDashboardController@destroy', $company->id], 'method'=> 'POST'])!!}
                                {{Form::hidden('_method', 'DELETE')}}
                                {{Form::submit('Delete', ['class'=> 'btn botton pull-right'])}}
                                {!!Form::close()!!} 
                            </div>
                            {{--  <script src="js/sweetalert.min.js"></script>
                            @include('sweet::alert')  --}}
                        @endif
                    @endif

    
    
                       
                    </div>
                </div>
                
                
            </div>

            
            
            
        </div>
           
    </div>
    
</section>

@endsection




