
@extends('layouts.app')
@section('content')

<img src = "../company_logo/back2back.PNG" alt="company logo" >
<div class="col-md-4 col-md-offset-4">
		
		
	<!-- <form class="form__container signup-form" method="POST" action="review" enctype="multipart/form-data"> -->
	{!! Form::open(['action' => ['AdminDashboardController@update', $company->id], 'method'=>'POST', 'enctype'=>'multipart/form-data', 'class'=>'form__container signup-form']) !!}
			{{ csrf_field() }}
		<div class="form-header">
			<h2>Business Registration Update</h2>
			<p>Connect your business to the global market</p>
		
		{{--  <img class= "card-img-top" src = "../company_logo/{{$company->name}}" alt="company logo" >  --}}
		
		</div> 

        <div class="form-group">
			{{Form::label('Company_name', 'Company Name')}}
		    {{Form::text('company_name',$company->company_name, ['class' => 'form-control', 'required'])}}
		</div>
	
		<div class ="form-group">
			{{Form::label('Contact_Person', 'Contact Person')}}
		    {{Form::text('contact_person', $company->contact_person, ['class'=> 'form-control','required'])}}
		</div>
        <div class="form-group">
		    {{Form::label('company_email', 'Email')}}
			{{Form::email('company_email', $company->company_email, ['class'=> 'form-control' ,'required'])}}
		</div>

		<div class="form-group">
		   	{{Form::label('mobile', 'Mobile contacts')}}
		   	{{Form::number('mobile', $company->mobile, ['class'=> "form-control", 'required'])}}
		</div>
			  
		<div class="form-group">
		   	{{Form::label('address', 'Address')}}
		   	{{Form::text('address', $company->address, ['class'=>'form-control', 'required'])}}
		</div>
			  
		<div class="form-group">
		  	{{Form::label('website', 'Website')}}
		  	{{Form::text('website', $company->website,['class'=> 'form-control', 'required'])}}
		</div>
		<div class="form-group ">
				<label name="services" class="form-check form-check-label">SERVICES</label>
				{{--  <select class="selectpicker" multiple data-actions-box="true">  --}}
				<select name="services" class = "selectpicker form-control" multiple>
					  <option name='services'>Tech Hub</option>
					 <option name='services'>Software Development</option>
					 <option name='services'>Website Develpment</option>
					 <option name='services'>Data Management</option>
					 <option name='services'>Android Development</option>
					 <option name='services'>iOS Development</option>
					 <option name='services'>Tech Training & Human Resources</option>
					 <option name='services'>Cloud storage</option>
					 <option name='services'>Virtual Reality</option>
					 <option name='services'>Digital Marketing</option>
					 <option name='services'>ICT Equipments</option>
					 <option name='services'>Co-working space</option>
				</select>
		
				
		</div>
		
			<div class="form-group">
			    {{Form::label('Business_details', 'Business Discription')}}
			   	{{Form::textarea('business_details', $company->business_details,['class'=> 'form-control', 'rows'=>'3'])}}
			</div><br/>

		
			<div class="form-group">
				<label for="company_logo">Update logo</label>
			    <input type="file" class="form-control-file" id="logo" name="image">
			</div>			
        	
			<div class="form-group">
				<label class="checkbox-inline"><input type="checkbox" required="required"> I accept the <a href="#">Terms of Use</a> &amp; <a href="#">Privacy Policy</a></label>
			</div>
		
			<div class="form-group">
                {{Form::hidden('_method', 'PUT')}}
				{{--  <input type="hidden" name="_token" value="{{csrf_token()}}">  --}}
				{{Form::Submit("Update",['class'=>"btn btn-primary button"])}}
		
	{!! Form::close() !!}

</div>

@endsection




