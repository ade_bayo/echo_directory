@extends('layouts.app')

@section('content')
<div class="login-form">
        <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}
                <div class="text-center social-btn">
                    {{--  <a href="{{url('login/facebook') }}" class="btn btn-primary btn-lg btn-block"><i class="fa fa-facebook"></i> Sign in with <b>Facebook</b></a>
                    <a href="{{url('login/twitter') }}" class="btn btn-info btn-lg btn-block"><i class="fa fa-twitter"></i> Sign in with <b>Twitter</b></a>  --}}
                    <a href="{{url('login/github') }}" class="btn btn-dark btn-lg btn-block"><i class="fa fa-github"></i> Sign in with <b>Github</b></a>
                </div>
                <div class="or-seperator"><b>or</b></div>
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    {{--  <label for="email" class="col-md-4 control-label">E-Mail Address</label>  --}}

                    <div>
                        <input id="email" type="email" class="form-control input-lg" placeholder="E-mail Address" name="email" value="{{ old('email') }}" required autofocus>

                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    {{--  <label for="password" class="col-md-4 control-label">Password</label>  --}}

                    <div>
                        <input id="password" type="password" class="form-control input-lg" placeholder="Password" name="password" required>

                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-offset-4">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                
                        <button type="submit" class="btn btn-success btn-lg btn-block login-btn">
                            Login
                        </button>

                        <a class="btn btn-link" href="{{ route('password.request') }}">
                            Forgot Your Password?
                        </a>
               
                </div>
            </form>
    
    <div class="text-center"><span class="text-muted">Dont have an account?</span> <a href="http://127.0.0.1:8000/register">Sign up here</a></div>
</div>

                <div class="panel-body">
                    
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
