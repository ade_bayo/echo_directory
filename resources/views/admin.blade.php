@extends('layouts.app')

@section('content')

    {{--  WELDONE  --}}
    <div class="container panel-body">
        <div class="panel-heading">
            <h3 class="panel-title">ADMIN DASHBOARD</h3>
        </div>

        
        <div class="col-md-3">
            <div class="well dash-box">
                <h2><span class="glyphicon glyphicon-flag" aria-hidden="true"></span>{{$pending_companies}}</h2>
                <h4>Pending</h4>
            </div>
        </div>
        <div class="col-md-3">
            <div class="well dash-box">
                <h2><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>{{$approved_companies}}</h2>
                <h4>Approved</h4>
            </div>
        </div>
        <div class="col-md-3">
            <div class="well dash-box">
                <h2><span class="glyphicon glyphicon-circle-arrow-down" aria-hidden="true"></span>{{$rejected_companies}}</h2>
                <h4>Rejected</h4>
            </div>
        </div>
        <div class="col-md-3">
            <div class="well dash-box">
            <h2><span class="glyphicon glyphicon-stats" aria-hidden="true"></span>{{$total_posts}}</h2>
            <h4>POSTS</h4>
        </div>
  
 
  
    </div>

    <section id="main">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="list-group">
                        <a href="index.html" class="list-group-item active main-color-bg"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                         Dashboard <span class="badge">12</span></a>
                        <a href="pages.html" class="list-group-item"><span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> Pages<span class="badge">25</span></a>
                        <a href="posts.html" class="list-group-item"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Posts<span class="badge"></span></a>
                        <a href="users.html" class="list-group-item"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Users <span class="badge"></span></a>
                    </div>

                </div>
            
       

                <div class="col-md-9">
                     <div class="panel panel-default">
  
                    <!--Latest User-->
                    <div class="panel panel-default">
                        <div class="panel-heading"style="background-color:  #095f59;">
                            <h3 class="panel-title">Latest Users</h3>
                            <div class="col-md-12 "> 
                                <div class="row">
                                    <div class="col-md-1 col-xs-6 col-sm-3">
                                        <h5>S/N</h5>
                                    </div>
                                    <div class="col-md-2 col-xs-6 col-sm-3">
                                        <h5>COMPANY DETAILS</h5>
                                    </div>
                                    <div class="col-md-2 col-xs-6 col-sm-3">
                                        <h5>CONTACTS</h5>
                                    </div>
                                    <div class="col-md-2 col-xs-6 col-sm-3">
                                        <h5>SERVICES</h5>
                                    </div>
                                    <div class="col-md-2 col-xs-6 col-sm-3">
                                        <h5>POSTED DATE</h5>
                                    </div>
                                    <div class="col-md-3 col-xs-6 col-sm-3">
                                        <h5>STATUS</h5>
                                    </div>
               
                                </div>
                            </div>
                        </div>
                  
                            <hr/>
                        @if(count($companies) > 0)
                            @foreach($companies as $company)
                            <div class="col-md-12 ">
                                <div class="row">
                                    <div class="col-md-1 col-xs-6 col-sm-3">
                   
                                        <h5>{{$company->id}}</h5>
    
                                    </div>
                                    <div class="col-md-2 col-xs-6 col-sm-3">
                  
                                        <h4>{{$company->company_name}}</h4>
                                        <h5>{{$company->address}}</h5>  
                                    </div>
                                    <div class="col-md-2 col-xs-6 col-sm-3">
                    
                                        <p>{{$company->email}}</p>
                                        <p>{{$company->website}}</p>
                                        <p>{{$company->mobile}}</p>
                                    </div>
                                    <div class="col-md-2 col-xs-6 col-sm-2">
                   
                                        <p>{{$company->services}}</p>
                    
                                    </div>
                                    <div class="col-md-2 col-xs-6 col-sm-3">
                 
                                        <p>{{ $company->created_at->toFormattedDateString()}}</p>
                                    </div>
                                    <div class="col-md-3 col-xs-6 col-sm-4">
                                        <a href="{{url('/companies/'.$company->id.'/reject')}}" class="rejected__botton btn-sm">Rejected</a> <!--SEND REJECTION MAIL TO USER-->
                                        <a href="{{url('/companies/'.$company->id.'/approve')}}" class="approved__botton btn-sm">Approved</a>

                                    </div>
                
                                </div>
                                 <hr>
                            </div>
                            @endforeach 
                        @endif
                        </div>
                    </div>
                </div>
            </div>
            {{$companies->links()}}
        </div>
    </section>


@endsection
   
         
     
