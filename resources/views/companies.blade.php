@extends('layouts.app')

@section('content')
      <body class="container">
          <div class= "category__display">
            <section class="category__banner">
              <div>
                <h1 class= "category__banner__text">Grow you business with professional tech companies around Nigeria </h1>
                <p class= "category__banner__p">Connect with expert tech firms around you to improve your business productivity</p>
              </div>
              <div class="search__container">
                <form action="/search" method="POST" role="search">
                    {{ csrf_field() }}
                    
                    <div class="form-row align-items-center">
                        <div class="col-md-8">
                            <label class="sr-only" for="inlineFormInputName">Search by Title</label>
                            <input type="text" class="form-control mb-2 mb-sm-0"name="q" id="inlineFormInputName" placeholder="Search by name">
                        </div>

                        {{--  <div class="col-md-4">
                            <select class="form-control form-control-lg">
                                  <option>Select category</option>
                                  <option>Tech Hubs </option>
                                  <option>Tech Training </option>
                                  <option>Data Management</option>
                                  <option>Web & Software</option>
                                  <option>Android & Mobile</option>
                            </select>
                        </div>  --}}
                   
                        <div class="col-md-3">
                            <button type="submit" class="btn btn-primary">Search</button>
                        </div>
                    </div>
                </form>
              </div> 
            </section>
          </div>
            <section>
                <div class="index-content">
                  
                    @foreach($companies as $company)
                      <a href="/companies/{{$company->id}}">
                        <div class="col-lg-3">
                          <div class="card">
                            <img class="card-img-top" src="../company_logo/{{$company->name}}" alt="company logo" height="60px" width=auto;>
                            <h5><a href="/companies/{{$company->id}}">{{$company->company_name}}</a></h5>
                            <p>{{$company->services}}</p>
                            <div class = "card__body">
                              <span>{{$company->mobile}}</span><br>
                              <span>{{$company->website}}</span><br>
                              <span>{{$company->company_email}}</span>
                            </div>
                            <hr>
                            <a href="/companies/{{$company->id}}" class="blue-button">Read More</a>
                          </div>
                        </div>
                      </a>
                    @endforeach
                    
                  
          
                </div>  
            </section>
              {{$companies->links()}}
      </body>
@endsection




