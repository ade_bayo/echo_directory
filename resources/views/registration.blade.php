
@extends('layouts.app')
@section('content')

	
<div class="col-md-4 col-md-offset-4">
	 {{--  <form class="form__container signup-form" method="POST" action="review" enctype="multipart/form-data">   --}}
	{!! Form::open(['action' => 'CompanyController@store', 'method'=>'POST', 'enctype'=>'multipart/form-data', 'class'=>'form__container signup-form']) !!}
			{{ csrf_field() }}
		<div class="form-header">
			<h2>Business Registration</h2>
			<p>Connect your business to the global market</p>
		</div>

        <div class="form-group">
			<label>Company Name</label>
		    <input type="text" class="form-control" name="company_name" id="company_name" placeholder="INITS Limited" required>
		</div>
	
		<div class ="form-group">
			<label>Contact Person</label>
		    <input type="text" class="form-control" name="contact_person" id="contact_person" placeholder="Dennis" required>
		</div>
        <div class="form-group">
		    <label>Company Email</label>
			<input type="email" class="form-control" name="company_email"  id="company_email" placeholder=" info@initsng.com" required>
		</div>

		<div class="form-group">
		   	<label for ="mobile">Mobile contacts</label>
		   	<input type="number" class="form-control" name="mobile"  id="mobile" placeholder="+234-807-787-008" required>
		</div>
			  
		<div class="form-group">
		   	<label for="address">Address</label>
		   	<input type="text" class="form-control" name="address"  id="address" placeholder="Head Office:16, Majaro Street, Onike, Yaba, Lagos." required>
		</div>
			  
		<div class="form-group">
		  	<label for="website">website</label>
		  	<input type="text" class="form-control" name="website" id="website" placeholder="http://initsng.com" required>
		</div>
		<div class="form-group ">
			<label required name="services" class="form-check form-check-label">SERVICES</label>
			{{--  <select class="selectpicker" multiple data-actions-box="true">  --}}
			<select name="services" class = "selectpicker form-control" multiple>
  				<option name='services'>Tech Hub</option>
 				<option name='services'>Software Development</option>
 				<option name='services'>Website Develpment</option>
 				<option name='services'>Data Management</option>
 				<option name='services'>Android Development</option>
 				<option name='services'>iOS Development</option>
 				<option name='services'>Tech Training & Human Resources</option>
 				<option name='services'>Cloud storage</option>
 				<option name='services'>Virtual Reality</option>
 				<option name='services'>Digital Marketing</option>
 				<option name='services'>ICT Equipments</option>
 				<option name='services'>Co-working space</option>
			</select>
	
			
		</div>
		<div>
			<div class="form-group">
			    <label for="business_details">Business Discription</label>
			    <textarea class="form-control" name="business_details"  id="business_details" rows="3" required></textarea>
			</div><br/>

			<div class="form-group">
				<label for="company_logo">Company logo</label>
			    <input type="file" class="form-control-file" id="logo" name="image" required>
			</div>
        	
			<div class="form-group">
				<label class="checkbox-inline"><input type="checkbox" required="required"> I accept the <a href="#">Terms of Use</a> &amp; <a href="#">Privacy Policy</a></label>
			</div>
		
			<div class="form-group">
				<input type="hidden" name="_token" value="{{csrf_token()}}">
			<button type="submit" name="submit" class="btn btn-primary" value="submit">Submit</button>
		</div>
	{!! Form::close() !!}
		

</div>

@endsection



