<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialProvider extends Model
{
    // protected $filliable =['provider_id', 'provider'];
    protected $guarded = [];
    
    
    public function user()
    {
        return $this->belongsTo('App\User');
        // return $this->belongsTo(User::class);
    }
}
