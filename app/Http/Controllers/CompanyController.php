<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use Illuminate\Http\File;
use Illuminate\Support\Facades\Input;
use App\Addfirm;
use Alert;

class CompanyController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
       return view('registration');   
    }

    public function dashboard()
    {
        return view('dashboard');
    } 
    public function home(){
        return view('welcome');
    }
    public function index(){
        $user_id = auth()->$user()->id;
        $user = User::find($user_id);
        return view('dashboard')->with('company', $user->company);
    }

    public function store(Request $request)
    { 
        $this->validate($request, [
            'company_name'=> 'required',
            'contact_person'=> 'required',
            'company_email'=> 'required',
            'mobile'=> 'required',
            'address'=> 'required',
            'website'=> 'required',
            'services'=> 'required',
            'business_details'=>'required'
        ]);
        $company= new Addfirm;
        $company->id=input::get('id');
        $company->user_id=auth()->user()->id;
        $company->company_name=input::get('company_name');
        $company->contact_person=input::get('contact_person');
        $company->company_email=input::get('company_email');
        $company->mobile=input::get('mobile');
        $company->address=input::get('address');
        $company->website=input::get('website');
        $company->services=input::get('services');
        $company->business_details=input::get('business_details');
        // $company->created_at=input::get('created_at');

        if (input::hasFile('image'))
        {
            $file=input::file('image');
            $file->move(public_path('company_logo').'/', $file->getClientOriginalName());
            $company->name = $file->getClientOriginalName();
            $company->size = $file->getClientsize();
            $company->type = $file->getClientMimeType();
            
        }
        $company->save();

        // Alert::success('COMPANY Successfully added!')->persistent("Close");
        return view('/review')->with('success', 'Company profile create');  /* SEND RECEIVED NOTIVICATION EMAIL TO REGISTERED COMPANY */
    }

}
