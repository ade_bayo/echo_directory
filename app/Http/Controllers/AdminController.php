<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use Illuminate\Http\File;
use Illuminate\Support\Facades\Input;
use App\Addfirm;

class AdminController extends Controller
{
    public function __construct()
    {
        
     $this->middleware('auth:admin');
    }

    public function index()
    {
       return view('admin');   
    }

    // public function create(){
    //     return view ('company.create');
    // }
    public function create()
    {
       return view('registration');   
    }

    


    // public function store(Request $request)
    // { 
    //     $this->validate($request, [
    //         'company_name'=> 'required',
    //         'contact_person'=> 'required',
    //         'company_email'=> 'required',
    //         'mobile'=> 'required',
    //         'address'=> 'required',
    //         'website'=> 'required',
    //         'services'=> 'required',
    //         'business_details'=>'required'
    //     ]);
    //     $company= new Addfirm;
    //     $company->id=input::get('id');
    //     $company->company_name=input::get('company_name');
    //     $company->contact_person=input::get('contact_person');
    //     $company->company_email=input::get('company_email');
    //     $company->mobile=input::get('mobile');
    //     $company->address=input::get('address');
    //     $company->website=input::get('website');
    //     $company->services=input::get('services');
    //     $company->business_details=input::get('business_details');
    //     // $company->created_at=input::get('created_at');

    //     if (input::hasFile('image'))
    //     {
    //         $file=input::file('image');
    //         $file->move(public_path().'/', $file->getClientOriginalName());
    //         $company->name = $file->getClientOriginalName();
    //         $company->size = $file->getClientsize();
    //         $company->type = $file->getClientMimeType();
            
    //     }
    //     $company->save();
    //     return view('review');  /* SEND RECEIVED NOTIVICATION EMAIL TO REGISTERED COMPANY */
    // }

    /*DISPLAY VIEW TO ADMIN FOR (DIS)APPROVAL*/
    public function showallAdmin()
    {  
        $companies = Addfirm::where('status', 0)->orderBy('created_at','desc')->paginate(10);
        $pending_companies = Addfirm::where('status', 0)->count();
        $approved_companies = Addfirm::where('status', 1)->count();
        $rejected_companies = Addfirm::where('status', 2)->count();
        $total_posts = Addfirm::where('id')->count();
        // dd($pending_companies);

       return view ('admin', compact('companies', 'pending_companies','rejected_companies','approved_companies', 'total_posts'));
    
    }
}