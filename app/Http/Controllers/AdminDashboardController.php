<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use Alert;

use Illuminate\Http\Request;
use App\Addfirm;
class AdminDashboardController extends Controller

{
 
    /*DISPLAY DETAILS TO THE USERS VIEW */
     public function approvedCompany(Addfirm $company)
    {    
        // $selectedCompany = Addfirm::where('id', $company)->first();
        // return $selectedCompany;
        // $company->status = 1;
        // $company->save();
        $company->update(['status'=> 1]);
        return back();
    }


    public function rejectedCompany(Addfirm $company)
    {
        // $selectedCompany = Addfirm::where('id', $company)->first();
        // return $selectedCompany;
        $company->update(['status'=> 2]);
        return back(); //SEND REJECTED MAIL TO THE USER
        

        // $companys = Addfirm::where('status', 2)->get();
        //     return view('rejected', compact('companys'));

    }  


    public function index(Addfirm $company){
        $companies = Addfirm::where ('status', 1)->orderBy('created_at','desc')->paginate(16);
        
        // $companies = Addfirm::orderBy('created_at', 'desc')->paginate(12);        
        return view('companies',compact('companies'));
    }
    
    public function rejected(){
        $companies = Addfirm::where ('status', 2)->orderBy('created_at','desc')->paginate(10);
        return view('companies',compact('companies'));
    }

    public function show($id)
    {
       $company = Addfirm::find($id);
       return view('company')->with('company', $company);

    } 

    public function edit($id)
    {
        $company = Addfirm::find($id);

       return view('companyupdate')->with('company', $company);

    }
    
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'company_name'=> 'required',
            'contact_person'=> 'required',
            'company_email'=> 'required',
            'mobile'=> 'required',
            'address'=> 'required',
            'website'=> 'required',
            'services'=> 'required',
            'business_details'=>'required'
        ]);
        $company= Addfirm::find($id);
        $company->company_name= $request->input('company_name');
        $company->contact_person=$request->input('contact_person');
        $company->company_email=$request->input('company_email');
        $company->mobile=$request->input('mobile');
        $company->address=$request->input('address');
        $company->website=$request->input('website');
        $company->services=$request->input('services');
        $company->business_details=$request->input('business_details');
        // $company->created_at=input::get('created_at');

        if (input::hasFile('image'))
        {
            $file=input::file('image');
            $file->move(public_path().'/', $file->getClientOriginalName());
            $company->name = $file->getClientOriginalName();
            $company->size = $file->getClientsize();
            $company->type = $file->getClientMimeType();
            
        }
        $company->save();
        // return flash('Your page is updated ');
        return view('review');
    }

   
    public function destroy($id)
    {
        alert()->warning('Are you sure you want to delete ?', 'WARNING!')->persistent('yes!');
        $company= Addfirm::find($id);
        $company->delete();
        return redirect('/companies');
            
    }
  
}

