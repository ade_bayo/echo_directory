<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Addfirm extends Model
{
    // public $table = 'addfirm';

    
    public $fillable =['user_id', 'company_name', 'contact_person', 
    'company_email', 'mobile', 'website', 'address', 'services', 
    'business_details','name','type','size', 'status'];

    // public function user()
    // {
    // return $this->belongTo('App\User');
    // }
}
