<?php

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();



//USER DASHBOARD   
Route::get('/dashboard', 'CompanyController@dashboard');  
Route::get('/registration', 'CompanyController@create');
Route::post('/review', 'CompanyController@store');

//ADMIN CONTROLLER
// Route::get('/admin', 'AdminController@showallAdmin'); //Accessible to ADMIN alone


Route::prefix('admin')->group(function() {
    Route::get('/login','Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::get('logout/', 'Auth\AdminLoginController@logout')->name('admin.logout');
    Route::get('/', 'AdminController@showallAdmin')->name('admin.dashboard');
   });

//ADMIN DASHBOARD CONTROLLER
Route::get('companies/{company}/approve', 'AdminDashboardController@approvedCompany');
Route::get('companies/{company}/reject', 'AdminDashboardController@rejectedCompany');
Route::resource('companies', 'AdminDashboardController');
Route::get('company', 'AdminDashboardController@show');
Route::get('rejected', 'AdminDashboardController@rejected');
// Route::get('companies', 'ApprovedController@index');



Route::get('login/github', 'Auth\LoginController@redirectToProvider');
Route::get('login/github/callback', 'Auth\LoginController@handleProviderCallback');

// Route::prefix('admin')->group(function() 
// {
// Route::get('/login', 'AdminAuth\LoginController@showLoginForm')->name('admin-auth.login');
// Route::post('/login', 'AdminAuth\LoginController@login'); 
// Route::post('/logout','AdminAuth\LoginController@logout')->name('admin.logout');
// Route::post('/password/email','AdminAuth\ForgotPasswordController@sendResetLinkEmail');
// Route::post('/password/reset', 'AdminAuth\ResetPasswordController@reset');
// Route::get('/password/reset', 'AdminAuth\ForgotPasswordController@showLinkRequestForm');
// Route::get('/password/reset/{token}', 'AdminAuth\ResetPasswordController@showResetForm');
// Route::post('/register','AdminAuth\RegisterController@register');

// });
  