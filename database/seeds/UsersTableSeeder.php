<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;
use App\User;
// class App\UsersTableSeeder extends Seeder
class UsersTableSeeder extends Seeder
{
    public function run()
    {
        $user = new User();
        $user->name = 'Mustapha Adebayo';
        $user->email = 'adebayo@initsng.com';
        $user->password = crypt('secret', '');
        $user-> save();

    }
}
