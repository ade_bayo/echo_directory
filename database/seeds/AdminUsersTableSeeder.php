<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;
use App\AdminUser;

class AdminUsersTableSeeder extends Seeder
{
    public function run()
    {
        $user = new AdminUser();
        $user->name = 'King George';
        $user->email = 'king@initsng.com';
        $user->password = crypt('secret', '');
        $user-> save();
    }
}
