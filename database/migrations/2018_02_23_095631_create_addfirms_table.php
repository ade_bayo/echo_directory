<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddfirmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addfirms', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('company_name');
            $table->string('contact_person');
            $table->string('company_email');
            $table->integer('mobile');
            $table->string('address');
            $table->string('website');
            $table->string('services');
            $table->string('business_details');
            $table->file('name');
            $table->string('type');
            $table->string('size');
            $table->integer('status')->default(0)->comment('default-0, approved-1, rejected-2');
            $table->timestamps();
        });
      
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addfirms');
    }
}
